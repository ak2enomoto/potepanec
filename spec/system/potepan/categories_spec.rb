require 'rails_helper'

RSpec.describe "カテゴリーページ画面", type: :system do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxon1) { create(:taxon, name: "Bags", taxonomy: taxonomy) }
  let(:taxon2) { create(:taxon, name: "Mugs", taxonomy: taxonomy) }
  let!(:product_with_taxon1) { create(:product, name: "Tote bag", taxons: [taxon1]) }
  let!(:another_product_with_taxon1) { create(:product, name: "Sholder bag", taxons: [taxon1]) }
  let!(:another_product_with_taxon2) { create(:product, name: "Normal Mug", taxons: [taxon2]) }

  context "パスにtaxon1.idを使用した場合" do
    before do
      visit  potepan_category_path(id: taxon1.id)
    end

    it "カテゴリーページのタイトルが正しいこと" do
      expect(page).to have_title "#{taxon1.name} | BIGBAG Store"
    end

    it "page_headerのリンクが正しいこと" do
      within ".breadcrumb" do
        expect(page).to have_link "HOME"
        expect(page).to have_content taxon1.name
        click_on "HOME"
      end
      expect(page).to have_current_path potepan_index_path
    end

    it "カテゴリー名と該当する商品数が表示されていること" do
      within ".side-nav" do
        expect(page).to have_content taxonomy.name
        expect(page).to have_content taxon1.name
        expect(page).to have_content taxon2.name
        expect(page).to have_content "(#{taxon1.products.count})"
        expect(page).to have_content "(#{taxon2.products.count})"
        click_on taxon2.name
      end
      expect(page).to have_current_path potepan_category_path(id: taxon2.id)
    end

    it "該当する商品(product_with_taxon1)の情報と個別ページのリンクがあること" do
      within all(:css, ".productBox")[0] do
        expect(page).to have_content product_with_taxon1.name
        expect(page).to have_content product_with_taxon1.display_price
        expect(page).not_to have_content another_product_with_taxon1.name
        expect(page).not_to have_content another_product_with_taxon2.name
        click_on product_with_taxon1.name
      end
      expect(page).to have_current_path potepan_product_path(product_with_taxon1.id)
    end

    it "該当する商品(another_product_with_taxon1)の情報と個別ページのリンクがあること" do
      within all(:css, ".productBox")[1] do
        expect(page).to have_content another_product_with_taxon1.name
        expect(page).to have_content another_product_with_taxon1.display_price
        expect(page).not_to have_content product_with_taxon1.name
        expect(page).not_to have_content another_product_with_taxon2.name
        click_on another_product_with_taxon1.name
      end
      expect(page).to have_current_path potepan_product_path(another_product_with_taxon1.id)
    end
  end

  context "パスにtaxon2.idを使用した場合" do
    before do
      visit  potepan_category_path(id: taxon2.id)
    end

    it "カテゴリーページのタイトルが正しいこと" do
      expect(page).to have_title "#{taxon2.name} | BIGBAG Store"
    end

    it "page_headerのリンクが正しいこと" do
      within ".breadcrumb" do
        expect(page).to have_link "HOME"
        expect(page).to have_content taxon2.name
      end
    end

    it "該当する商品(another_product_with_taxon2)の情報と個別ページのリンクがあること" do
      within ".productBox" do
        expect(page).to have_content another_product_with_taxon2.name
        expect(page).to have_content another_product_with_taxon2.display_price
        expect(page).not_to have_content product_with_taxon1.name
        expect(page).not_to have_content another_product_with_taxon1.name
        click_on another_product_with_taxon2.name
      end
      expect(page).to have_current_path potepan_product_path(another_product_with_taxon2.id)
    end
  end
end
