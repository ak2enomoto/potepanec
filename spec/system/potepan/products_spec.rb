require 'rails_helper'

RSpec.describe "商品詳細画面", type: :system do
  let(:taxon1) { create(:taxon) }
  let(:taxon2) { create(:taxon) }
  let(:product) { create(:product, name: "Tote bag", taxons: [taxon1]) }
  let!(:related_products) { create_list(:product, 4, taxons: [taxon1]) }
  let(:product_with_taxon2) { create(:product, name: "Shirts", taxons: [taxon2]) }
  let!(:related_products_with_taxon2) { create_list(:product, 5, taxons: [taxon2]) }

  context "パスにproduct.idを使用した場合" do
    before do
      visit  potepan_product_path(id: product.id)
    end

    it "カテゴリーページのタイトルが正しいこと" do
      expect(page).to have_title "#{product.name} | BIGBAG Store"
    end

    it "page_headerのリンクが正しいこと" do
      within ".breadcrumb" do
        expect(page).to have_link "HOME"
        expect(page).to have_content product.name
        click_on "HOME"
      end
      expect(page).to have_current_path potepan_index_path
    end

    it "productの商品情報とリンクが正しく表示されていること" do
      within ".media-body" do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
        expect(page).to have_content product.description
        expect(page).not_to have_content product_with_taxon2.name
        expect(page).not_to have_content related_products.first.name
        click_on "一覧ページへ戻る"
      end
      expect(page).to have_current_path potepan_category_path(id: taxon1.id)
    end

    it "related_productsの商品情報とリンクだけ表示されていること" do
      within ".productsContent" do
        expect(page).to have_content related_products.first.name
        expect(page).to have_content related_products.first.display_price
        expect(page).not_to have_content product_with_taxon2.name
        expect(page).not_to have_content product.name
        click_on related_products.first.name
      end
      expect(page).to have_current_path potepan_product_path(id: related_products.first.id)
    end
  end

  context "パスにproduct_with_another_taxon.idを使用した場合" do
    before do
      visit  potepan_product_path(id: product_with_taxon2.id)
    end

    it "カテゴリーページのタイトルが正しいこと" do
      expect(page).to have_title "#{product_with_taxon2.name} | BIGBAG Store"
    end

    it "page_headerのリンクが正しいこと" do
      within ".breadcrumb" do
        expect(page).to have_link "HOME"
        expect(page).to have_content product_with_taxon2.name
        click_on "HOME"
      end
      expect(page).to have_current_path potepan_index_path
    end

    it "product_with_taxon2の商品情報とリンクが正しく表示されていること" do
      within ".media-body" do
        expect(page).to have_content product_with_taxon2.name
        expect(page).to have_content product_with_taxon2.display_price
        expect(page).to have_content product_with_taxon2.description
        expect(page).not_to have_content product.name
        expect(page).not_to have_content related_products_with_taxon2.first.name
        click_on "一覧ページへ戻る"
      end
      expect(page).to have_current_path potepan_category_path(id: taxon2.id)
    end

    it "related_products_with_taxon2が4件に絞れていること" do
      within ".productsContent" do
        expect(all(:css, ".productBox").size).to eq(4)
      end
    end
  end
end
