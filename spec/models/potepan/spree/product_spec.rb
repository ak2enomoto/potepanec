require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let(:taxon1)  { create(:taxon) }
  let(:taxon2)  { create(:taxon) }
  let(:product) { create(:product, name: "Tote bag", taxons: [taxon1]) }
  let!(:related_product) { create(:product, name: "Mugs", taxons: [taxon1]) }
  let!(:product_with_another_taxon) { create(:product, name: "Shirts", taxons: [taxon2]) }

  it "関連商品のみが得られること" do
    expect(product.related_products).to match_array(related_product)
    expect(product.related_products).not_to include(product)
    expect(product.related_products).not_to include(product_with_another_taxon)
  end

  it "関連商品がない場合、空の配列を返すこと" do
    expect(product_with_another_taxon.related_products).to match_array([])
  end
end
