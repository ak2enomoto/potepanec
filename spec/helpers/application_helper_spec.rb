require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title" do
    context "page_titleがnilの時" do
      it "タイトルはbase_titleのみであること" do
        expect(full_title(nil)).to eq "BIGBAG Store"
      end
    end

    context "page_titleが空の時" do
      it "タイトルはbase_titleのみであること" do
        expect(full_title("")).to eq "BIGBAG Store"
      end
    end

    context "page_titleに文字が入っていた時" do
      it "タイトルは(文字 | base_title)の形になっていること" do
        expect(full_title("RUBY ON RAILS TOTE")).to eq "RUBY ON RAILS TOTE | BIGBAG Store"
      end
    end
  end
end
