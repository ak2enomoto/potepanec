require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :request do
  describe "GET /potepan/products" do
    let(:taxon1) { create(:taxon) }
    let(:taxon2) { create(:taxon) }
    let(:product) { create(:product, name: "Tote bag", taxons: [taxon1]) }
    let!(:related_product) { create(:product, name: "Mugs", taxons: [taxon1]) }
    let(:product_with_another_taxon) { create(:product, name: "Shirts", taxons: [taxon2]) }

    context "パスにproduct.id使用した場合" do
      before do
        get potepan_product_path(id: product.id)
      end

      it "レスポンスが正しく返ってくること" do
        expect(response).to have_http_status(200)
      end

      it "productとrelated_productだけが表示されていること" do
        expect(response.body).to include product.name
        expect(response.body).to include related_product.name
        expect(response.body).not_to include product_with_another_taxon.name
      end
    end

    context "パスにproduct_with_another_taxon.id使用した場合" do
      before do
        get potepan_product_path(id: product_with_another_taxon.id)
      end

      it "レスポンスが正しく返ってくること" do
        expect(response).to have_http_status(200)
      end

      it "product_with_another_taxonだけが表示されていること" do
        expect(response.body).to include product_with_another_taxon.name
        expect(response.body).not_to include related_product.name
        expect(response.body).not_to include product.name
      end
    end
  end
end
