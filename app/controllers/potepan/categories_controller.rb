class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = Spree::Product.in_taxons(@taxon).includes(master: [:images, :default_price])
    @taxonomies = Spree::Taxonomy.includes(:taxons)
  end
end
