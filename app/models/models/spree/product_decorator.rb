Spree::Product.class_eval do
  def related_products
    if taxons.present?
      Spree::Product.in_taxons(taxons).
        where.not(id: id).distinct
    else
      Spree::Product.none
    end
  end
end
